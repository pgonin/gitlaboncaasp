---
title: 'Deploying and configuring GitLab on SUSE CaaS Platform - platform fixes'
author:
    - Paul Gonin
geometry: margin=1in
header-includes: |
    \usepackage{graphicx}
    \usepackage{titling}
    \usepackage{fancyhdr}
    \pagestyle{fancyplain}
    \fancyhead{}
    \rhead{\fancyplain{}{\thetitle}}
    \lfoot{\raisebox{-0.5\height}{\includegraphics[width=1in]{pdf/suse-logo.png}}}
    \cfoot{}
    \rfoot{\thepage}

...

# Setup ntp on jumphost & Mirror server

Access jumphost (password is "linux")
and install ntpd
``` bash
ssh root@192.168.42.4
zypper in ntp
```

Edit /etc/ntp.conf
setup the following ntp server
``` bash
[...]
server 0.opensuse.pool.ntp.org iburst
server 1.opensuse.pool.ntp.org iburst
server 2.opensuse.pool.ntp.org iburst
server 3.opensuse.pool.ntp.org iburst
[...]
```

Start ntp service
``` bash
systemctl start ntpd.service
systemctl enable ntpd.service
```

Check time is synchronized well
``` bash
date
```

Access mirror (password is "linux")
and install chrony
``` bash
ssh root@192.168.42.3
zypper in chrony
```

Edit /etc/chrony.conf
setup the following ntp server
``` bash
[...]
pool us.pool.ntp.org iburst
[...]
```


Start ntp service
``` bash
systemctl start chronyd.service
systemctl enable chronyd.service
```


# Setup certificates on mirror server and update jumphost

On mirror, there are 3 vhosts
mirror.ho1415.net for rmt
registry.ho1415.net for local registry server
charts.ho1415.net for helm charts mirror


Access mirror (password is "linux")
``` bash
ssh root@192.168.42.3

rm *.cert *.csr *.key
```

Create the following certificate.config file

```
[ req ]
default_bits        = 2048
default_keyfile     = charts-server-key.pem
distinguished_name  = subject
req_extensions      = req_ext
x509_extensions     = x509_ext
string_mask         = utf8only
prompt              = no

# The Subject DN can be formed using X501 or RFC 4514 (see RFC 4519 for a description).
#   Its sort of a mashup. For example, RFC 4514 does not provide emailAddress.
[ subject ]
countryName         = DE
stateOrProvinceName     = Bavaria
localityName            = Nuremberg
organizationName         = SUSE GmbH

# Use a friendly name here because it's presented to the user. The server's DNS
#   names are placed in Subject Alternate Names. Plus, DNS names here is deprecated
#   by both IETF and CA/Browser Forums. If you place a DNS name here, then you
#   must include the DNS name in the SAN too (otherwise, Chrome and others that
#   strictly follow the CA/Browser Baseline Requirements will fail).
commonName          = *.ho1415.net
emailAddress            = contact@ho1415.net

# Section x509_ext is used when generating a self-signed certificate. I.e., openssl req -x509 ...
[ x509_ext ]

subjectKeyIdentifier        = hash
authorityKeyIdentifier    = keyid,issuer

# You only need digitalSignature below. *If* you don't allow
#   RSA Key transport (i.e., you use ephemeral cipher suites), then
#   omit keyEncipherment because that's key transport.
basicConstraints        = CA:FALSE
keyUsage            = digitalSignature, keyEncipherment
subjectAltName       = @alternate_names
nsComment           = "OpenSSL Generated Certificate"

# RFC 5280, Section 4.2.1.12 makes EKU optional
#   CA/Browser Baseline Requirements, Appendix (B)(3)(G) makes me confused
#   In either case, you probably only need serverAuth.
# extendedKeyUsage    = serverAuth, clientAuth

# Section req_ext is used when generating a certificate signing request. I.e., openssl req ...
[ req_ext ]
subjectKeyIdentifier        = hash
basicConstraints        = CA:FALSE
keyUsage            = digitalSignature, keyEncipherment
subjectAltName          = @alternate_names
nsComment           = "OpenSSL Generated Certificate"

# RFC 5280, Section 4.2.1.12 makes EKU optional
#   CA/Browser Baseline Requirements, Appendix (B)(3)(G) makes me confused
#   In either case, you probably only need serverAuth.
# extendedKeyUsage    = serverAuth, clientAuth

[ alternate_names ]
DNS.1       = charts.ho1415.net
DNS.2       = registry.ho1415.net
DNS.3       = mirror.ho1415.net
```

Create certificate and key
``` bash
openssl req -new -config certificate.config > charts.cert.csr
openssl rsa -in charts-server-key.pem -out charts.cert.key
openssl x509 -in charts.cert.csr -out charts.cert.cert -req \
  -signkey charts.cert.key -days 365

rm /etc/rmt/ssl/charts.cert.cert /etc/rmt/ssl/charts.cert.key
cp charts.cert.cert /etc/rmt/ssl/
cp charts.cert.key /etc/rmt/ssl/
```

change vhosts configuration
in /etc/nginx/vhosts.d/registry-server-https.conf & /etc/nginx/vhosts.d/charts-server-https.conf
[...]
ssl_certificate     /etc/rmt/ssl/charts.cert.cert;
ssl_certificate_key /etc/rmt/ssl/charts.cert.key;
[...]

Restart nginx
``` bash
systemctl restart nginx
```


Access jumphost (password is "linux")
``` bash
ssh root@192.168.42.4

cd /usr/share/pki/trust/anchors
rm *.pem
openssl s_client -showcerts -servername charts.ho1415.net -connect charts.ho1415.net:443 > charts_CA_Cert.pem
openssl s_client -showcerts -servername registry.ho1415.net -connect registry.ho1415.net:443 > registry_CA_Cert.pem
update-ca-certificates
```

Test with curl
``` bash
curl https://registry.ho1415.net
curl https://charts.ho1415.net
```
