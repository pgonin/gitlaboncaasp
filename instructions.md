---
title: 'Deploying and configuring Gitlab on SUSE CaaS Platform'
author:
    - Paul Gonin
geometry: margin=1in
header-includes: |
    \usepackage{graphicx}
    \usepackage{titling}
    \usepackage{fancyhdr}
    \pagestyle{fancyplain}
    \fancyhead{}
    \rhead{\fancyplain{}{\thetitle}}
    \lfoot{\raisebox{-0.5\height}{\includegraphics[width=1in]{pdf/suse-logo.png}}}
    \cfoot{}
    \rfoot{\thepage}

...

Start Mirror Virtual Machine
============================

A virtual machine with RMT populated with SUSE CaaS Platform 3 updates as well as a charts mirror and local registry populated with Gitlab containers and charts.

Check that virtual machines network is up => network should be in Active state.
``` bash
virsh net-info HO1415-net
```

``` bash
Name:           HO1415-net
UUID:           6b85daeb-b272-4521-8ef1-c852fab4ef85
Active:         yes
Persistent:     yes
Autostart:      yes
Bridge:         virbr-HO1415
```

Either use Virt-Manager and start VM HO1415-Mirror
or use virsh
``` bash
virsh start HO1415-Mirror
```

[check services on mirror]


Start SUSE CaaS Platform Admin
==============================

First step of getting the SUSE CaaS Platform ready is to start the admin node and configure it.

Either use Virt-Manager and start VM HO1415-CaaSP-Admin
or use virsh
``` bash
virsh start HO1415-CaaSP-Admin
```

With a browser, access the Admin node configuration page.
https://192.168.42.5/

Click "Create an account"
Fill fields email address and password (ex. ho1415@ho1415.net and suselinux)

On next step "Initial CaaS Platform Configuration"
For 'Internal Dashboard Location' set caasp-admin.ho1415.net
Check 'Install Tiller' (Helm's server component)
Leave other choices unchanged.
Click Next at the bottom of the page.

On 'Bootstrap your CaaS Platform' page click 'Next'


You'll reach the "Select nodes and roles" page.
Next step is to setup Master and Worker nodes

Setup SUSE CaaS Platform Master and Worker nodes
================================================

Either use Virt-Manager and start VM HO1415-CaaSP-Master HO1415-CaaSP-Node1 HO1415-CaaSP-Node2 HO1415-CaaSP-Node3
or use virsh
``` bash
virsh start HO1415-CaaSP-Master
virsh start HO1415-CaaSP-Node1
virsh start HO1415-CaaSP-Node2
virsh start HO1415-CaaSP-Node3
```

After a few minutes, the 4 nodes should appear in Velum / CaaSP Admin dashboard in 'Pending Nodes' display

Once the 4 nodes are listed in the Pending Nodes display, click 'Accept All Nodes'

Once the nodes are prepared for Roles selection in the upper display, assign Master and Worker roles (using hostname as guide)
  then click Next

On the 'Confirm bootstrap' page
  set External Kubernetes API FQDN  to caasp-master.ho1415.net
  and External Dashboard FQDN to caasp-admin.ho1415.net
  then click Next


Mirror certificate
==================

Access Mirror Vm (password is linux)
``` bash
ssh root@192.168.42.3
cat /etc/rmt/ssl/rmt-ca.crt
```

Open Velum https://192.168.42.5
Click Settings and then  'System wide certificates'
Set the name Mirror and copy the content of /etc/rmt/ssl/rmt-ca.crt
Click Save
And then 'Apply changes'


Register systems to local RMT
=============================

Access Admin Node VM (password is n0ts3cr3t)
``` bash
docker exec $(docker ps -q --filter name=salt-master) salt -C "* and not L@ca" cmd.run 'SUSEConnect --url https://mirror.ho1415.net'
```
The output should look like
``` bash
admin:
    Registered CAASP 3.0 x86_64
    To server: https://mirror.ho1415.net
    Successfully registered system.
143857735bf9445fad994c576a1dbf51:
    Registered CAASP 3.0 x86_64
    To server: https://mirror.ho1415.net
    Successfully registered system.
5a4ed2c58eb94f57ad406596ea7b0430:
    Registered CAASP 3.0 x86_64
    To server: https://mirror.ho1415.net
    Successfully registered system.
f5bdcd5e7259413584dabf1fd317ba25:
    Registered CAASP 3.0 x86_64
    To server: https://mirror.ho1415.net
    Successfully registered system.
6ce7e4bde0134ce7be11ab53dd3272dd:
    Registered CAASP 3.0 x86_64
    To server: https://mirror.ho1415.net
    Successfully registered system.
```

Update nodes and Cluster
========================

Access Admin Node VM (password is n0ts3cr3t) and start transactional-update on all the nodes (+notify Velum that a new update is available)
``` bash
docker exec -it $(docker ps -q --filter name=salt-master) salt -P "roles:(admin|kube-(master|minion))" cmd.run '/usr/sbin/transactional-update cleanup dup reboot'
docker exec -it $(docker ps -q --filter name=salt-master) salt --force-color '*' saltutil.sync_all
```

Access Velum admin page
  Start with updating admin node
  Once completed, proceed with updating the 4 cluster nodes



Configure Jumphost
==================

Either use Virt-Manager and start VM HO1415-Jumphost
or use virsh
``` bash
virsh start HO1415-jumphost
```

On Velum, collect kubeconfig file

On 'Log in to Your Account' page, enter Velum admin credentials (ho1415@ho1415.net / suselinux)

Copy kubeconfig file to Jumphost (password is linux)
``` bash
scp kubeconfig root@192.168.42.4:~/kubeconfig
```

Check that access to kubernetes cluster is ok
``` bash
ssh root@192.168.42.4
export KUBECONFIG=~/kubeconfig
kubectl get nodes
```

Configure local registry
========================

Using Velum, setup the SUSE CaaS Platform to use a local containers registry:
Open Velum https://192.168.42.5
  Click Settings in the top menu bar
  Add Remote registry
  Set as name 'Remote GitLab'
  Set as URL https://registry.gitlab.com/

  Then 'Add Mirror'
  Mirror of 'Remote GitLab'
  Name: 'Local gitlab mirror'
  URL: 'https://registry.ho1415.net'

Apply changes


Configure Persistent Storage for GitLab containers
==================================================

When a dynamic storage provider is available (public cloud storage, Cinder/OpenStack, Ceph/SES...) the configuration of persistent storage volumes will be dynamic.

For this lab, there is no dynamic storage provider and we'll use local persistent volumes
https://kubernetes.io/blog/2018/04/13/local-persistent-volumes-beta/

In this configuration containers will use local storage from the nodes.
This is suitable for testing, but not production grade configuration.

Containers will running on the host providing the storage, wo we'll distribute storage (and containers) statically on the different nodes.

caasp-worker1        gitaly prometheus
caasp-worker2        minio redis
caasp-worker3        postgresql

On jumphost, in directory /root/gitlab-on-caasp there are Persistent Volume configuration files

They look like
``` bash
apiVersion: v1
kind: PersistentVolume
metadata:
  name: gitlab-minio
spec:
  capacity:
    storage: 10Gi
  accessModes:
  - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  volumeMode: Filesystem
  storageClassName: gitlab-local-storage
  local:
    path: /opt/gitlab/minio
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - caasp-worker2
```

On each worker the directories that are defined in the configuration must be created

We will create the directories on each worker

caasp-worker1: (password is n0ts3cr3t)
``` bash
ssh root@192.168.42.11
mkdir -p /opt/gitlab/gitaly
mkdir -p /opt/gitlab/prometheus
```

caasp-worker2: (password is n0ts3cr3t)
``` bash
ssh root@192.168.42.12
mkdir -p /opt/gitlab/minio
mkdir -p /opt/gitlab/redis
```

caasp-worker3: (password is n0ts3cr3t)
``` bash
ssh root@192.168.42.13
mkdir -p /opt/gitlab/postgresql
```

Next stpe is creation of Local Storage StorageClass provisioner for GitLab

File ~/gitlab-on-caasp/gitlab-local-storage.yaml describes the Storage class
``` bash
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: gitlab-local-storage
provisioner: kubernetes.io/no-provisioner
volumeBindingMode: WaitForFirstConsumer
```

The Storage class is to be created from jumphost with command:
``` bash
export KUBECONFIG=~/kubeconfig
kubectl create -f ~/gitlab-on-caasp/gitlab-local-storage.yaml
```

Then Local Persistent Volumes can be created
``` bash
kubectl create -f ~/gitlab-on-caasp/gitlab-postgresql-pv.yaml
kubectl create -f ~/gitlab-on-caasp/gitlab-gitaly-pv.yaml
kubectl create -f ~/gitlab-on-caasp/gitlab-minio-pv.yaml
kubectl create -f ~/gitlab-on-caasp/gitlab-redis-pv.yaml
kubectl create -f ~/gitlab-on-caasp/gitlab-prometheus-pv.yaml
```


Initialize helm
===============

From jumphost system, initialize helm to use local helm charts mirror

``` bash
export KUBECONFIG=~/kubeconfig
helm repo add gitlab https://charts.ho1415.net/charts
helm repo update
```

Configure Cluster Role Bindid PSP for nginx
===========================================

``` bash
kubectl create -f gitlab-rolebinding-nginx.yaml
```

This command applies this configuration
``` bash
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: gitlab-nginx-ingress-psp-privileged
  namespace: default
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: suse:caasp:psp:privileged
subjects:
- kind: ServiceAccount
  name: gitlab-nginx-ingress
  namespace: default
```

Deploy GitLab CE using Gitlab chart
===================================

Helm now can be used to install containers and configure resources in the CaaSP cluster

``` bash
helm install --name gitlab gitlab/gitlab \
--timeout 600 \
--set global.hosts.domain=caasp-worker1.ho1415.net \
--set global.hosts.externalIP=192.168.42.11 \
--set controller.service.externalIPs=192.168.42.11 \
--set certmanager-issuer.email=pgonin@suse.de \
--set global.edition=ce \
--set gitlab.migrations.image.repository=registry.gitlab.com/gitlab-org/build/cng/gitlab-rails-ce \
--set gitlab.sidekiq.image.repository=registry.gitlab.com/gitlab-org/build/cng/gitlab-sidekiq-ce \
--set gitlab.unicorn.image.repository=registry.gitlab.com/gitlab-org/build/cng/gitlab-unicorn-ce \
--set gitlab.unicorn.workhorse.image=registry.gitlab.com/gitlab-org/build/cng/gitlab-workhorse-ce \
--set gitlab.task-runner.image.repository=registry.gitlab.com/gitlab-org/build/cng/gitlab-task-runner-ce
```

Once deployment is done, Persistent Volumes Claims need to be replaced with claims to local volume storage previously created

``` bash
kubectl delete pvc repo-data-gitlab-gitaly-0
kubectl delete pvc gitlab-minio
kubectl delete pvc gitlab-postgresql
kubectl delete pvc gitlab-prometheus-server
kubectl delete pvc gitlab-redis

kubectl create -f gitlab-gitaly-pvc.yaml
kubectl create -f gitlab-minio-pvc.yaml
kubectl create -f gitlab-postgresql-pvc.yaml
kubectl create -f gitlab-prometheus-pvc.yaml
kubectl create -f gitlab-redis-pvc.yaml
```

To check the progress of deployment
``` bash
kubectl get pods
```

Since we don't have a load balancer service as in public clouds, we need to set the public ip for the ingress controller on one of the workers
``` bash
kubectl get services gitlab-nginx-ingress-controller
NAME                              TYPE           CLUSTER-IP     EXTERNAL-IP   PORT(S)                                   AGE
gitlab-nginx-ingress-controller   LoadBalancer   172.24.89.21   <pending>     80:32087/TCP,443:31071/TCP,22:31028/TCP   12h
```

If the external IP is not correctly set, it can be set with the following command
``` bash
kubectl patch svc gitlab-nginx-ingress-controller -p '{"spec":{"externalIPs":["192.168.42.11"]}}'
```



Remove & clean the deployment
=============================

```
helm delete --purge gitlab
kubectl delete pvc repo-data-gitlab-gitaly-0

```

``` bash
kubectl delete pvc repo-data-gitlab-gitaly-0
kubectl delete pvc gitlab-minio
kubectl delete pvc gitlab-postgresql
kubectl delete pvc gitlab-prometheus-server
kubectl delete pvc gitlab-redis

kubectl delete pv gitlab-gitaly
kubectl delete pv gitlab-minio
kubectl delete pv gitlab-postgresql
kubectl delete pv gitlab-prometheus
kubectl delete pv gitlab-redis
```


Congrats !!
